import {Reponse} from "../class/Reponse";

export default  interface QuestionToClient {
    question: string;
    choix: Reponse[];
    url: string;
    indexQuestion: string;
}
