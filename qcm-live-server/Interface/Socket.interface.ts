import Room from "../class/Room";
import User from "../class/User";
import QuestionToClient from "./QuestionToClient.interface";
import {Stat} from "../class/Stat";
import {Question} from "../class/Question";


export interface ServerToClientEvents {
    noArg: () => void;
    basicEmit: (a: number, b: string, c: Buffer) => void;
    withAck: (d: string, callback: (e: number) => void) => void;
    userId: (e: string) => void;
    idRoom: (e: string) => void;
    infoRoom: (user: Room) => void;
    users: (users: User[]) => void;
    nbPlayersChanged: (nbPlayers: number) => void;
    nbTimesChanged: (nbTimes: number) => void;
    nbQuestionsChanged: (nbQuestions: number) => void;
    themeChanged: (theme: string[]) => void;
    isBanned: () => void;
    pong: () => void;
    startGame: () => void;
    question: (question: QuestionToClient) => void;
    timeLeft: (timeLeft: number) => void;
    collectAnswers: () => void;
    gameOver: () => void;
    classement: (result: {
        user: string,
        score: number
    }[]) => void;
    roomDeleted: (roomId: string) => void;
    themes: (themes: string[]) => void;
    maxPlayer: (message: string) => void;
    stats: (stats: {
        stat: Stat,
        reponseindex: number
    }) => void;
}

export interface ClientToServerEvents {
    hello: () => void;
    rouge: () => void;
    createRoom: (roomName: string) => void;
    createUser: (e: string) => void;
    joinRoom: (roomId: string) => void;
    getInfoRoom: (roomId: string) => void;
    askUsers: (roomId: string) => void;
    userReady: (data: { roomId: string, userId: string, isReady: boolean }) => void;
    onChangePlayers: (data: { roomId: string, nbPlayers: number }) => void;
    onChangeTimes: (data: { roomId: string, nbTimes: number }) => void;
    onChangeQuestions: (data: { roomId: string, nbQuestions: number }) => void;
    onChangeTheme: (data: { roomId: string, theme: string }) => void;
    ban: (data: { roomId: string, userId: string }) => void;
    userLeave: () => void;
    ping: () => void;
    start: (roomId: string) => void;
    answer: (data: { roomId: string, answer: string }) => void;
    leaveRoom: (idRoom: string) => void;
    themes: (roomId: string) => void;
    receiveJson: (data: { roomId: string, questionsGood: Question[] }) => void;
}

export interface InterServerEvents {
    ping: () => void;
}

export interface SocketData {
    name: string;
    age: number;
}
