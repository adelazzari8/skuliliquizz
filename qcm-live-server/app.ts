import {Server} from "socket.io";
import {initSocketServer} from './config/socket.config'
import * as http from 'http'

const io = new Server({cors: {origin: "*"},});
let server = http.createServer().listen(3000, '100.74.7.85');

initSocketServer(io)
io.listen(server);
