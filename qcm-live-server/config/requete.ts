import {Question} from '../class/Question';
import {Reponse} from '../class/Reponse';
import {Stat} from '../class/Stat';
import {getTheme, Theme} from '../class/Theme';
import * as mysql from 'mysql'

// const mysql = require('mysql');
//ne pas toucher ce fichier only ajout de requete

export default class Requete {
    connection: any;

    constructor() {
        this.connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'GOg269jLMIh',
            database: 'QUESTIONS',
            port: 3306

            // host: 'devbdd.iutmetz.univ-lorraine.fr',
            // user: 'burnel12u_appli',
            // password: 'Justin55500',
            // database: 'burnel12u_QUESTIONS',
            // port: 3306
        });
    }

    getQuestions(): Promise<Question[]> {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Question ORDER BY RAND() LIMIT 20', (err: any, rows: Question[]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });
        });
    }

    getReponses(): Promise<Reponse[]> {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Reponse', (err: any, rows: Reponse[]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    getReponsesById(id_question: number): Promise<Reponse[]> {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Reponse WHERE id = ?', [id_question], (err: any, rows: Reponse[]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    getThemes(): Promise<getTheme[]> {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT labelle FROM Theme', (err: any, rows: getTheme[]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    getStats() {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Stat', (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    getStatsById(id: number): Promise<Stat[]> {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Stat WHERE id = ?', [id], (err: any, rows: Stat[]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    getQuestionsById(id: number) {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Question WHERE id_question = ?', [id], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    getQuestionsByTheme(themes: string[]): Promise<Question[]> {
        return new Promise((resolve, reject) => {
            if (themes.length === 0) {
                this.connection.query('SELECT * FROM Question ORDER BY RAND() LIMIT 20', (err: any, rows: Question[]) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(rows);
                    }
                });
            } else {
                let mesThemes = `(Theme.labelle="${themes[0]}"`
                themes.splice(0, 1)
                themes.forEach(theme => {
                    mesThemes += ` OR Theme.labelle="${theme}"`
                })
                mesThemes += `)`
                this.connection.query(`SELECT Question.labelle, Question.id_theme, Question.id_question, Question.url, Question.difficulte, Question.reponse_index FROM Question,Theme WHERE Theme.id_theme = Question.id_theme AND ${mesThemes} ORDER BY RAND() LIMIT 20`, (err: any, rows: Question[]) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(rows);
                    }
                });
            }

        });
    }

    countTheme(): Promise<number> {
        return new Promise((res) => {
            this.connection.query('SELECT COUNT(*) as nbTheme FROM Theme', (_err: any, rows: any) => {
                res(rows[0].nbTheme)

            })
        })
    }

    getQuestionsByDifficulte(difficulte: number) {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Question WHERE difficulte = ?', [difficulte], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });
        });
    }


    getThemesById(id_theme: number) {
        return new Promise((resolve, reject) => {
            this.connection.query('SELECT * FROM Theme WHERE id_theme = ?', [id_theme], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }


    insertQuestion(question: Question) {
        return new Promise((resolve, reject) => {
            this.connection.query('INSERT INTO Question (id_theme, labelle, url, difficulte, reponse_index) VALUES (?, ?, ?, ?,?)', [question.id_theme, question.labelle, question.url, question.difficulte, question.reponse_index], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    insertReponse(reponse: Reponse) {
        return new Promise((resolve, reject) => {
            this.connection.query('INSERT INTO Reponse (id, numero, labelle) VALUES (?, ?, ?)', [reponse.id, reponse.numero, reponse.labelle], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    insertTheme(theme: Theme) {
        return new Promise((resolve, reject) => {
            this.connection.query('INSERT INTO Theme (labelle) VALUES (?)', [theme.labelle], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    insertStat(stat: Stat) {
        return new Promise((resolve, reject) => {
            this.connection.query('INSERT INTO Stat (id, juste, faux) VALUES (?, ?, ?)', [stat.id, stat.juste, stat.faux], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    updateStat(stat: Stat) {
        return new Promise((resolve, reject) => {
            console.log("j'update une stat avec" + stat.juste + stat.faux)
            this.connection.query('UPDATE Stat SET juste = ?, faux = ? WHERE id = ?', [stat.juste, stat.faux, stat.id], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    deleteQuestion(id: number) {
        return new Promise((resolve, reject) => {
            this.connection.query('DELETE FROM Question WHERE id_question = ?', [id], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    deleteReponse(id: number) {
        return new Promise((resolve, reject) => {
            this.connection.query('DELETE FROM Reponse WHERE id = ?', [id], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    deleteTheme(id: number) {
        return new Promise((resolve, reject) => {
            this.connection.query('DELETE FROM Theme WHERE id_theme = ?', [id], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    deleteStat(id: number) {
        return new Promise((resolve, reject) => {
            this.connection.query('DELETE FROM Stat WHERE id = ?', [id], (err: any, rows: unknown) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });

        });
    }

    quitter() {
        this.connection.end();
    }

}


/* exemple d appel pour chercher toutes les questions avec la difficulté 1


let requete = new Requete();
let tab: Question[] = [];
let tab2=requete.getQuestionsByDifficulte(1);


tab2.then((result) => {
    tab = result as Question[];
    sauvegardeQuestionJson(tab);
    tab.forEach((question) => {
        console.log(question);
    });
});

requete.quitter();*/

