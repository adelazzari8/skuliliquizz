import { writeFile } from 'fs'
import { Question } from '../class/Question';
import { Stat } from '../class/Stat';

function sauvegardeQuestionJson(tab: Question[]) {
    writeFile('questions.json', JSON.stringify(tab), (err) => {
        if (err) {
            console.log(err);
        }
    });
}

function sauvegardeStatJson(tab: Stat[]) {
    writeFile('stats.json', JSON.stringify(tab), (err) => {
        if (err) {
            console.log(err);
        }
    });
}

