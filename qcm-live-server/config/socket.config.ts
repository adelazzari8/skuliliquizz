import {Server} from "socket.io";
import Room from "../class/Room";
import User from "../class/User";
import Requete from "./requete";
import {Reponse} from "../class/Reponse";
import {Question} from "../class/Question";
import {Stat} from "../class/Stat";
import {ClientToServerEvents, InterServerEvents, ServerToClientEvents, SocketData} from "../Interface/Socket.interface";

let rooms: Room[] = [];
let users: User[] = [];

export const initSocketServer = (io: Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>) => {
    console.log("Web Socket initializing...");

    io.on("connect", async (socket) => {
        console.log(socket.id, 'connected')
        // TODO : verify if user is in the list of user and if he has a pseudo / room or rediret to home page the client side

        socket.on('disconnect', () => {
            let index = users.findIndex((user) => user.id === socket.id);
            if (index === -1) return;
            const user = users[index];
            users.splice(index, 1);
            console.log('User disconnected', user.userName, user.idRoom);
            if (!user.idRoom) return;
            let room = <Room>rooms.find((room) => room.id === user.idRoom);
            leaveRoom(room);
        });

        socket.on("createUser", (userName: string) => {
            users.push(new User(socket.id, userName));
            socket.nsp.to(socket.id).emit('userId', socket.id);
        });

        socket.on("createRoom", (roomName: string) => {
            let nbUsers = users.length - 1
            let host: User = <User>users.find((user) => user.id === socket.id)
            let idRoom = generateRoomId()
            let room = new Room(idRoom, roomName, host);
            room.addUser(host);
            rooms.push(room);
            socket.join(idRoom);
            host.idRoom = room.id;
            socket.nsp.to(socket.id).emit('idRoom', room.id);
        });

        function generateRoomId(): string {
            let idRoom: number;
            do {
                idRoom = Math.floor(Math.random() * 90_000) + 10_000;
            } while (rooms.some(room => room.id === String(idRoom)));
            return String(idRoom);
        }

        socket.on("joinRoom", (roomId) => {
            let user = users.find((user) => user.id === socket.id)
            let room = rooms.find((room) => room.id === roomId)
            if (!room) return;
            if (!user) return;
            if (room.nbPlayers === room.users.length) {
                socket.nsp.to(socket.id).emit('maxPlayer', "soory trop de joueur");
            } else {
                room.users.push(user);
                socket.join(room.id);
                user.idRoom = room.id;
                socket.nsp.to(socket.id).emit('idRoom', roomId);
            }
        });

        socket.on("getInfoRoom", (roomId: string) => {
            let room: Room = <Room>rooms.find((room) => room.id === roomId)
            socket.nsp.to(socket.id).emit('infoRoom', room)
        });

        socket.on("askUsers", (roomId) => {
            let room: Room = <Room>rooms.find((room) => room.id === roomId)
            socket.nsp.to(room.id).emit("users", room.users)
        });

        socket.on("userReady", (data) => {
            let room: Room = <Room>rooms.find((room) => room.id === data.roomId)
            let user: User = <User>room.users.find((user) => user.id === socket.id)
            user.isReady = data.isReady;
            socket.nsp.to(room.id).emit('users', room.users);
        });

        socket.on("leaveRoom", (idRoom) => {
            let room = <Room>rooms.find(room => room.id === idRoom)
            if (!room) return;
            leaveRoom(room)
        });

        function leaveRoom(room: Room) {
            if (room.host.id !== socket.id) {
                room.users = room.users.filter((user) => user.id !== socket.id);
                console.log(room.users)
                socket.nsp.to(room.id).emit('users', room.users);
                return;
            }

            // user is the host
            socket.nsp.to(room.id).emit('roomDeleted', room.id);
            for (let user of room.users) {
                user.idRoom = "";
            }
            room.users = []
            rooms = rooms.filter((existRoom) => existRoom.id !== room.id);
            io.in(room.id).socketsLeave(room.id);

        }

        socket.on("ban", ({roomId, userId}) => {
            const room = rooms.find(room => room.id === roomId);
            if (!room) return;
            const userIndex = room.users.findIndex(user => user.id === userId);
            if (userIndex === -1) return;
            const bannedUser = room.users.splice(userIndex, 1)[0];
            socket.nsp.to(bannedUser.id).emit("isBanned");
            socket.nsp.to(room.id).emit("users", room.users);
        });

        function handleRoomChange(option: number, roomId: string, field: "nbPlayers" | "nbTimes" | "nbQuestions") {
            let room = <Room>rooms.find(room => room.id === roomId);
            room[field] = Number(option);
            socket.nsp.to(room.id).emit(`${field}Changed`, room[field]);
        }

        socket.on("onChangePlayers", (event) => handleRoomChange(event.nbPlayers, event.roomId, "nbPlayers"));
        socket.on("onChangeTimes", (event) => handleRoomChange(event.nbTimes, event.roomId, "nbTimes"));
        socket.on("onChangeQuestions", (event) => handleRoomChange(event.nbQuestions, event.roomId, "nbQuestions"));

        socket.on("themes", async (roomId: string) => {
            try {
                const requete = new Requete();
                const themes = await requete.getThemes();
                socket.nsp.to(roomId).emit("themes", themes.map(theme => theme.labelle));
                requete.quitter();
            } catch (error) {
                console.error(error);
            }
        })

        socket.on("onChangeTheme", ({roomId, theme}) => {
            const room = <Room>rooms.find(room => room.id === roomId);
            const themeIndex = room.themes.indexOf(theme);
            if (themeIndex === -1) {
                room.themes.push(theme);
            } else {
                room.themes.splice(themeIndex, 1);
            }
            socket.nsp.to(room.id).emit("themeChanged", room.themes);
        })

        socket.on("receiveJson", (data: { roomId: string, questionsGood: Question[] }) => {
            const room = rooms.find((room) => room.id === data.roomId);
            if (!room) return
            room.useJson = true
            room.questions = data.questionsGood
        })

        async function getQuestionFromDatabase(room: Room) {
            let questions: Question[] = []
            let requete = new Requete();
                await requete.getQuestionsByTheme(room.themes).then(res => {
                    res.forEach(ques => {
                        let q = new Question(ques.id_theme, ques.id_question, ques.labelle, ques.url, ques.difficulte, ques.reponse_index)
                        questions.push(q)
                    });
                });
            shuffle(questions).then(res => {
                questions = res
            });

            await getAnswerOfQuestion(questions).then(async res => {
                questions.length = room.nbQuestions
                room.questions = res;
            })
            requete.quitter()
        }

        socket.on("start", async (roomId: string) => {
            const room = rooms.find((room) => room.id === roomId);
            if (!room) return;

            if (!room.useJson) {
                await getQuestionFromDatabase(room)

            } else {
                room.nbQuestions = room.questions.length
            }

            socket.nsp.to(room.id).emit("startGame");
            await play(room)
            await sleep(1000)
            room!.users.forEach(user => {
                user.isReady = false
                user.score = 0
            })
        })

        async function play(room: Room) {
            room.questions.length = room.nbQuestions;
            room.indexQuestion = 0;

            while (room.indexQuestion < room.questions.length && room.users.length > 0) {
                const question = room.questions[room.indexQuestion];
                socket.nsp.to(room.id).emit("question", {
                    question: question.labelle,
                    choix: question.reponses,
                    url: question.url,
                    indexQuestion: `${room.indexQuestion + 1}/${room.nbQuestions}`,
                });

                socket.nsp.to(room.id).emit("timeLeft", room.nbTimes);
                await sleep(room.nbTimes * 1000);

                socket.nsp.to(room.id).emit("collectAnswers");

                let date1 = new Date();
                while (room.users.some((user) => !user.hasAnswer)) {
                    await sleep(500);
                    let time = new Date().getTime() - date1.getTime();
                    if (time > 6000) {
                        break;
                    }
                }

                if (!room.useJson) {
                    await stat(room)
                }
                room.indexQuestion += 1;
                room.users.forEach((user) => {
                    user.hasAnswer = false;
                });
                room.juste = 0;
                room.faux = 0;
            }
            await sleep(500);
            socket.nsp.to(room.id).emit("gameOver");
            setTimeout(() => {
                classement(room);
            }, 1000);
        }


        async function stat(room: Room) {
            let questionId = room?.questions[room.indexQuestion].id_question;
            let requete = new Requete();
            let stat: Stat = new Stat(Number(questionId), 0, 0);
            await requete
                .getStatsById(Number(questionId))
                .then(async (res) => {
                    if (res.length > 0) {
                        stat = new Stat(
                            Number(questionId),
                            Number(res[0].juste),
                            Number(res[0].faux)
                        );
                        stat.faux += room.faux;
                        stat.juste += room.juste;
                        await requete.updateStat(stat);
                    } else {
                        stat = new Stat(Number(questionId), 0, 0);
                        stat.faux += room.faux;
                        stat.juste += room.juste;
                        await requete.insertStat(stat);
                    }
                });

            await requete
                .getStatsById(Number(room?.questions[room.indexQuestion].id_question))
                .then((res) => {
                    stat = new Stat(0, Number(res[0].juste), Number(res[0].faux));
                });

            await requete.quitter();
            let reponseindex = +room.questions[room.indexQuestion].reponse_index;
            socket.nsp.to(room!.id).emit("stats", {stat, reponseindex});
            await sleep(3000);
        }

        socket.on("answer", async ({roomId, answer}) => {
            const room = rooms.find(room => room.id === roomId);
            if (!room) return
            let resQuestion = '';
            resQuestion = room.questions[room.indexQuestion].reponse_index
            const user = room.users.find(user => user.id === socket.id);
            if (!user) return
            if (answer === '-1') {
                user.hasAnswer = true
                return
            }
            if (resQuestion === answer) {
                user.score++;
                room.juste++;
            } else {
                room.faux++;
            }
            user.hasAnswer = true;
        })



        function classement(room: Room) {
            const result = room.users.sort((a, b) => b.score - a.score).map(user => {
                return {
                    user: user.userName,
                    score: user.score
                }
            })
            socket.nsp.to(room.id).emit("classement", result);
        }
        function StatExist(id: number) {
            let requete = new Requete();
            requete.getStatsById(id).then(res => {
                if (res.length == 0) {
                    createStat(id)
                }
            })
            requete.quitter
        }

        //test

        function createStat(question: number) {
            let stat: Stat = new Stat(question, 0, 0);
            let requete = new Requete();
            requete.insertStat(stat);
            requete.quitter();
        }

        async function getAnswerOfQuestion(questions: Question[]) {
            let requete = new Requete();
            for (let question of questions) {

                await requete.getReponsesById(question.id_question).then((result) => {
                    result.forEach(data => {
                        question.reponses.push(new Reponse(data.id, data.numero, data.labelle))
                    });
                })
            }

            requete.quitter();
            await shuffle(questions)
            return (questions)
        }
    })
}

const sleep = (time: number) => new Promise((resolve) => setTimeout(resolve, time));


async function shuffle(array: any[]): Promise<any[]> {
    return new Promise(
        (resolve) => {

            let currentIndex = array.length, randomIndex;

            // While there remain elements to shuffle.
            while (currentIndex != 0) {

                // Pick a remaining element.
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex--;

                // And swap it with the current element.
                [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
            }

            resolve(array);
        })
}
