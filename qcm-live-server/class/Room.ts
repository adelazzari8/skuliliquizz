import { Question } from "./Question"
import User from "./User"

export default class Room {
    id: string
    name: string
    nbPlayers: number = 10
    nbTimes: number = 7
    nbQuestions: number = 10
    host: User
    users: User[] = []
    questions: Question[]
    themes: string[] = []
    indexQuestion: number = 0
    juste=0
    faux=0

    useJson : boolean

    constructor(id: string, name: string, host: User, useJson = false) {
        this.id = id;
        this.name = name
        this.host = host
        this.useJson = useJson


    }

    addUser(user: User) {
        this.users.push(user)
    }
}
