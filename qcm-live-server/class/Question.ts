import { Reponse } from "./Reponse";

export class Question {
    done: boolean;
    id_theme: number;
    id_question: number;
    labelle: string;
    url: string;
    difficulte: number;
    reponses: Reponse[];
    reponse_index: string;
    constructor(id_theme: number, id_question: number, labelle: string, url: string, difficulte: number, reponse_index: string) {
        this.reponse_index = reponse_index;
        this.id_theme = id_theme;
        this.id_question = id_question;
        this.labelle = labelle;
        this.url = url;
        this.difficulte = difficulte;
        this.reponses = [];
        this.done = false;
    }
}
