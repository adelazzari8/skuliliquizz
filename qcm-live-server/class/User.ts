export default class User {
    id: string
    userName: string
    isReady: boolean = false
    idRoom: string = ""
    score = 0
    hasAnswer = false

    constructor(id: string, userName: string) {
        this.id = id
        this.userName = userName
    }

}