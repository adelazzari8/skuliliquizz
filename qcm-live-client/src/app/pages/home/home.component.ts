import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WebSocketService } from 'src/app/services/web-socket.service';
import { info } from 'src/assets/global-constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private socketServ: WebSocketService, private formBuilder: FormBuilder, private router: Router) { }

  pseudoForm!: FormGroup
  pseudo: string = ''
  ngOnInit(): void {
    this.pseudoForm = new FormGroup({
      pseudo: new FormControl(this.pseudo, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
        Validators.pattern('[^ ]*')
      ]),
    });
  }
  get name() { return this.pseudoForm.get('pseudo'); }

  addUser() {
    const pseudo = this.pseudoForm.get('pseudo')?.value
    this.socketServ.emit('createUser', pseudo)
    let receiveId = this.socketServ.listen('userId').subscribe((id: string) => {
      if (!id) return
      info.userId = id;
      receiveId.unsubscribe()
      this.goToMenu()
    })
  }

  goToMenu() {
    this.router.navigate(['/menu'])
  }
  goToCreateQuizz() {
    this.router.navigate(['/create-quizz'])
  }
}
