import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-options-bar',
  templateUrl: './options-bar.component.html',
  styleUrls: ['./options-bar.component.scss']
})
export class OptionsBarComponent implements OnInit {
  @Input() value: number = 0;
  @Input() max: number = 20
  @Input() min: number = 2
  @Input() name: string = ""

  @Output() changedValue: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit(): void {
  }

  changeValue(term: string) {
    this.value = +term
  }

  changeValueToServer(term: string) {
    this.changedValue.emit(+term)
  }

  // onValuechange(value : string){
  //   console.log(value)
  //   this.changedValue = +value
  // }

}
