import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WebSocketService} from 'src/app/services/web-socket.service';
import {fullScreen, info} from 'src/assets/global-constants';
import {Question} from "../../../../../class/Question";

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})

export class OptionsComponent implements OnInit {
  @Output() ready: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() params!: { nbPlayers: number, nbTimes: number, nbQuestions: number, themes: string[] }
  @Input() isHost: boolean = false
  @Input() isAllReady: boolean = false

  isReady = false
  fullScreen = fullScreen
  themes: string[] = []

  jsonIsImported = false
  hasJsonError = false

  messageTab: string[] = ["Import Quizz", 'info']


  constructor(private socketService: WebSocketService) {
  }

  ngOnInit(): void {

    console.log(this.params);
    console.log('les params');
    if (this.isHost) {
      this.socketService.emit("themes", info.roomId)
      this.socketService.listen("themes").subscribe((data: string[]) => {
        this.themes = data
      })
    }

    this.socketService.listen("nbPlayersChanged").subscribe((data: number) => {
      this.params.nbPlayers = data
    })
    this.socketService.listen("nbTimesChanged").subscribe((data: number) => {
      this.params.nbTimes = data
    })
    this.socketService.listen("nbQuestionsChanged").subscribe((data: number) => {
      this.params.nbQuestions = data
    })
    this.socketService.listen("themeChanged").subscribe((data: string[]) => {
      console.log('on est laaaaaaa', data)
      this.params.themes = data
    })


  }

  toggleReady() {
    // laisser comme ça pour mobile
    this.isReady = !this.isReady
    this.ready.emit(this.isReady);

  }

  onPlayersChange(ev: number) {
    let nbPlayers = ev
    let roomId = info.roomId
    this.socketService.emit("onChangePlayers", {roomId, nbPlayers})
  }

  onTimesChange(ev: number) {
    let roomId = info.roomId
    let nbTimes = ev
    this.socketService.emit("onChangeTimes", {roomId, nbTimes})
  }

  onQuestionsChange(ev: number) {
    let roomId = info.roomId
    let nbQuestions = ev
    this.socketService.emit("onChangeQuestions", {roomId, nbQuestions})
  }

  onThemeChange(theme: string) {
    let roomId = info.roomId
    this.socketService.emit("onChangeTheme", {roomId, theme})
  }


  start() {
    this.socketService.emit('start', info.roomId);
  }

  isChecked(theme: string): boolean {
    let th = this.params.themes.find(th => th === theme)
    return !!th
  }

  async importJson(event: any) {
    let obj = JSON.parse(await event.target.files[0].text())
    console.log(obj)
    this.jsonValidator(obj)
  }

  buttonStatus(): string {
    if (this.hasJsonError) {
      return 'danger'
    } else if (this.jsonIsImported) {
      return 'success'
    }
    return 'info'
  }

  buttonMessage(): string {
    if (this.hasJsonError) {
      return 'Error in the json file'
    } else if (this.jsonIsImported) {
      return 'Quizz imported'
    }
    return 'Import Quizz'
  }

  jsonValidator(questions: any) {
    let questionsGood: Question[] = []
    try {
      questions.forEach((el: Question) => {
        let q = new Question(el.id_question)
        q.url = el.url ? el.url : ""
        q.labelle = el.labelle
        q.reponses = el.reponses
        q.reponse_index = el.reponse_index
        q.done = el.done ? el.done : false
        questionsGood.push(q)
      })
      let roomId = info.roomId
      this.socketService.emit("receiveJson", {roomId, questionsGood})
      this.hasJsonError = false
      this.jsonIsImported = true
    } catch (e) {
      this.hasJsonError = true
      this.jsonIsImported = false
    } finally {
      this.messageTab[1] = this.buttonStatus()
      this.messageTab[0] = this.buttonMessage()
    }
  }

}
