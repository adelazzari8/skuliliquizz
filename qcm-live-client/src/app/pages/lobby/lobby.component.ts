import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WebSocketService } from 'src/app/services/web-socket.service';
import { fullScreen, info } from 'src/assets/global-constants';

import Room from 'src/class/Room';
import User from 'src/class/User';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})

export class LobbyComponent implements OnInit {
  fullScreen = fullScreen
  isReady = false
  isHost = false
  isAllReady = false
  host = ''
  isLoaded = false
  users: User[] = []
  roomId: string = info.roomId
  params: { nbPlayers: number, nbTimes: number, nbQuestions: number, themes:string[]} = { nbPlayers: 0, nbTimes: 0, nbQuestions: 0,themes: [] }

  constructor(private socketService: WebSocketService, private activeRoute: ActivatedRoute, private router: Router) {
  }
  ngOnInit(): void {
    let startGame = this.socketService.listen('startGame').subscribe(() => {
      startGame.unsubscribe();
      infoRoom.unsubscribe();
      users.unsubscribe();
      deleteRoom.unsubscribe();
      this.router.navigate(['/party']);
    })
    console.log('doing');

    this.socketService.emit('getInfoRoom', info.roomId)
    let infoRoom = this.socketService.listen('infoRoom').subscribe((room: Room) => {
      if (!room) return;
      this.host = room.host.id;
      if (this.host === info.userId) {
        this.isHost = true;
      }
      this.params.nbPlayers = room.nbPlayers
      this.params.nbTimes = room.nbTimes
      this.params.nbQuestions = room.nbQuestions
      this.params.themes = room.themes
      this.isLoaded = true;
    })

    this.socketService.emit('askUsers', info.roomId)
    let users = this.socketService.listen('users').subscribe((users: User[]) => {
      if (!users) return
      this.users = users
      let noReady = 0
      users.forEach(user => {
        if (!user.isReady) {
          noReady++
          return
        }
      });
      this.isAllReady = noReady === 0;
    })

    //? Need to stay here because of unsubscribe
    let deleteRoom = this.socketService.listen('roomDeleted').subscribe((idRoom: string) => {
      this.socketService.emit("deleteRoom", idRoom);
      startGame.unsubscribe();
      infoRoom.unsubscribe();
      users.unsubscribe();
      deleteRoom.unsubscribe();
      console.log('chuis delete')
      this.router.navigate(['/menu'])
    })
  }

  toggleReady() {
    console.log('toogle le ready')
    this.isReady = !this.isReady
    let { roomId, userId } = info
    this.socketService.emit('userReady', { roomId, userId, isReady: this.isReady })
  }

  start() {
    this.socketService.emit('start', info.roomId);
  }

  goToHome() {
    this.router.navigate(['home'])
  }



}
