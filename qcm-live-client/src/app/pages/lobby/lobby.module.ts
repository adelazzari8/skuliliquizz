import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LobbyComponent } from './lobby.component';
import {NbButtonModule, NbCardModule, NbCheckboxModule, NbIconModule, NbRadioModule} from '@nebular/theme';
import { JoueursComponent } from './components/joueurs/joueurs.component';
import { OptionsComponent } from './components/options/options.component';
import { LoaderComponent } from 'src/app/component/loader/loader.component';
import { LoaderModule } from 'src/app/component/loader/loader.module';
import { OptionsBarComponent } from './components/options/options-bar/options-bar.component';
import {ClipboardModule} from "@angular/cdk/clipboard";



@NgModule({
  declarations: [LobbyComponent, JoueursComponent, OptionsComponent, OptionsBarComponent],
    imports: [
        CommonModule,
        NbCardModule,
        NbIconModule,
        NbButtonModule,
        NbRadioModule,
        LoaderModule,
        ClipboardModule,
        NbCheckboxModule
    ]
})
export class LobbyModule { }
