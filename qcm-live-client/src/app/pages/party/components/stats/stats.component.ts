import {Component, Input, OnInit} from '@angular/core';
import {WebSocketService} from "../../../../services/web-socket.service";
import {Stat} from "../../../../../class/Stat";

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

@Input() bonneReponse = 0
@Input() mauvaisReponse = 0

  constructor(private socket: WebSocketService) {
  }

  ngOnInit(): void {

  }

  verify(verify : number){
    return (verify !== 0 && !isNaN(verify) )
  }
}
