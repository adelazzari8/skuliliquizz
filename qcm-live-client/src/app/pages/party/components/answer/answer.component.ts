import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {fullScreen} from 'src/assets/global-constants';
import {Reponse} from '../../../../../class/Reponse';
import {WebSocketService} from "../../../../services/web-socket.service";
import {Stat} from "../../../../../class/Stat";

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent implements OnInit, OnChanges {
  fullScreen = fullScreen


  showStat: boolean = false; //setter
  @Input() choix: Reponse[] = [];
  indexReponse: number = -1;
  @Output() reponseindex: EventEmitter<number> = new EventEmitter<number>();

  bonneReponse: string = "#007d6c"
  mauvaiseReponse: string = "#94124e"
  BLUE: string = '#3366FF'

  cliqued: string = 'rgba(51, 102, 255, 0.1)'

  ouJeClique: boolean[] = [false, false, false, false]
  wichColor: string[] = [this.BLUE, this.BLUE, this.BLUE, this.BLUE]

  temp: number = -1

  constructor(private socket: WebSocketService) {
  }

  ngOnInit(): void {
    this.socket.listen("stats").subscribe((stats: { stat: Stat, reponseindex: number }) => {
      this.showStat = true;
      this.indexReponse = stats.reponseindex
      this.onStat()
    });

  }

  ngOnChanges(change: SimpleChanges) {
    for (let i = 0; i <= this.ouJeClique.length - 1; i++) {
      this.ouJeClique[i] = false
      this.wichColor[i] = this.BLUE
    }
    this.indexReponse = -1
    this.showStat = false;
  };


  onAnswer(index: number) {
    this.reponseindex.emit(index);
    for (let i = 0; i <= this.ouJeClique.length - 1; i++) {
      this.ouJeClique[i] = false
      this.wichColor[i] = this.BLUE
    }
    this.ouJeClique[index] = true
    this.wichColor[index] = this.cliqued
    this.temp = index

  }

  onStat() {
    console.log('laa')
    for (let i = 0; i < this.wichColor.length; i++) {
      if (i === this.indexReponse) {
        // console.log('bonne reponse')
        this.wichColor[i] = this.bonneReponse
      } else if (this.ouJeClique[i]) {
        this.wichColor[i] = this.mauvaiseReponse
        // console.log('bonne reponse')
      } else {
        this.wichColor[i] = this.BLUE
      }
    }
  }


//   console.log('loooool')
//   //bonne réponse
//   if (index === this.indexReponse) {
//     if (index === this.temp) {
//       this.temp = -1
//     }
//     return this.bonneReponse
//
//   }
//
//   if (index === this.temp) {
//     this.temp = -1
//     return this.mauvaiseReponse
//   }
//
//   return ''
// }
//
// sleep = (milliseconds: number) => {
//   return new Promise(resolve => setTimeout(resolve, milliseconds))
// }
}
