import {Component, OnInit} from '@angular/core';
import {WebSocketService} from 'src/app/services/web-socket.service';
import {fullScreen, info} from 'src/assets/global-constants';
import {Router} from '@angular/router';
import {Reponse} from '../../../class/Reponse';
import {Stat} from '../../../class/Stat';

interface CurrentQuestion {
  question: string,
  choix: Reponse[],
  url: string,
  indexQuestion: string
}

@Component({
  selector: 'app-party',
  templateUrl: './party.component.html',
  styleUrls: ['./party.component.scss']
})

export class PartyComponent implements OnInit {

  actualQuestion: CurrentQuestion = {
    question: 'Quel est le meilleur framework javascript ?',
    choix: [new Reponse(1, 1, 'Ma grand mère'), new Reponse(1, 2, 'choix2'), new Reponse(1, 3, 'choix3'), new Reponse(1, 4, 'choix4')],
    url: 'https://via.placeholder.com/200',
    indexQuestion: '1/10'
  };

  fullScreen = fullScreen;
  time = 0;
  answer = -1;
  stat: Stat = new Stat(0, 0, 0);
  showStat: boolean = false;

  bonneReponse = 0
  mauvaisReponse = 0


  constructor(private socketService: WebSocketService, private router: Router) {
  }

   ngOnInit(): void {
    let collecteAnswer = this.socketService.listen('collectAnswers').subscribe(async () => {
      this.socketService.emit('answer', {roomId: info.roomId, answer: `${this.answer}`});
      this.answer = -1


    });

     this.socketService.listen("stats").subscribe((stats: { stat: Stat, reponseindex: number }) => {
       let nbTot = stats.stat.faux + stats.stat.juste
       this.bonneReponse = Math.round(stats.stat.juste * 100 / nbTot)
       this.mauvaisReponse = 100 - this.bonneReponse
       this.showStat = true
     });



    let gameOver = this.socketService.listen('gameOver').subscribe(() => {
      collecteAnswer.unsubscribe();
      gameOver.unsubscribe();
      question.unsubscribe();
      timeLeft.unsubscribe();
      this.router.navigate(['resultat']);
    });

    let question = this.socketService.listen('question').subscribe((questionObj: CurrentQuestion) => {
      this.showStat = false
      this.actualQuestion = questionObj;
    });

    let timeLeft = this.socketService.listen('timeLeft').subscribe(async (time: number) => {
      if (!time) return
      await this.timer(time)
    });
  }

  async timer(time: number) {
    for (let i = time; i > 0; i--) {
      this.time = i;
      await this.sleep(1000);
    }
  }

  sleep = (time: number) => new Promise((resolve) => setTimeout(resolve, time));

  onResponseIndex(event: any) {
    this.answer = event;
  }

  goToHome() {
    this.router.navigate(['home'])
  }

}
