import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CreationQuizzComponent} from "./creation-quizz.component";
import { QuestionCardComponent } from './components/question-card/question-card.component';
import {NbButtonModule, NbCardModule, NbIconModule, NbInputModule, NbRadioModule} from "@nebular/theme";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [CreationQuizzComponent, QuestionCardComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbInputModule,
    FormsModule,
    NbButtonModule,
    NbRadioModule,
    NbIconModule,
  ]
})
export class CreationQuizzModule { }
