import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Question} from "../../../../../class/Question";

@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.scss'],
  animations: [
    trigger('placeholder', [

      state('typing', style({
        left: '0px',
        top: '-12px',
        opacity: 1,
        color: '#FFFFFF'
      })),
      state('unTyping', style({
        left: '15px',
        top: '29px',
        opacity: 1,
        color: '#8f9bb3'
      })),


      transition('typing => unTyping', [
        animate('300ms ease-in-out', style({transform: 'translate(15px,41px)', color: '#FFFFFF'}))
      ]),
      transition('unTyping => typing', [
        animate('300ms ease-in-out', style({transform: 'translate(-15px,-41px)', color: '#8f9bb3'}))
      ])

    ])
  ]
})
export class QuestionCardComponent implements OnInit {

  @Output() title: EventEmitter<string> = new EventEmitter<string>()
  @Output() goodAnwser: EventEmitter<number> = new EventEmitter<number>()

  @Output() deleteQuestion : EventEmitter<Question> = new EventEmitter<Question>()

  @Input() question!: Question

  typing: string = 'unTyping'
  option: any;


  constructor() {
  }

  ngOnInit(): void {
  }

  onFocus() {
    this.typing = 'typing'
  }

  onBlur(title: HTMLInputElement) {
    if (title.value.replace(' ', '')) return
    this.typing = 'unTyping'
  }

  onGoodAnswerChange(goodAnswer: number) {
    this.question.reponse_index = String(goodAnswer);
  }

  onDeleteQuestion(){
    this.deleteQuestion.emit(this.question)
  }
}
