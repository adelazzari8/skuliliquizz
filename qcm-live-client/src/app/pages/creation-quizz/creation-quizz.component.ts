import {Component, OnInit} from '@angular/core';
import {Question} from "../../../class/Question";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {DomSanitizer} from "@angular/platform-browser";
import {Router} from "@angular/router";

@Component({
  selector: 'app-creation-quizz',
  templateUrl: './creation-quizz.component.html',
  styleUrls: ['./creation-quizz.component.scss'],
  animations: [
    trigger('superAnime', [
      state('in', style({opacity: 1})),
      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [
        style({opacity: 0}),
        animate(200)
      ]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave',
        animate(200, style({opacity: 0, width : '2px'})))
    ])
  ]
})
export class CreationQuizzComponent implements OnInit {

  questions: Question[] = []

  constructor(private sanitizer: DomSanitizer, private router: Router) {
    this.questions.push(this.createQuestion())
  }

  ngOnInit(): void {
  }

  createQuestion(): Question {
    let tabId = this.questions.map(question => question.id_question)
    const max = (tabId.length !== 0) ? tabId.reduce((a, b) => {
      return Math.max(a, b)
    }) : -1;
    return new Question(max + 1)
  }

  onAddQuestion() {
    this.questions.push(this.createQuestion())
  }

  onRmAllQuestions() {
    this.questions = [this.createQuestion()];
  }

  onDeleteQuestion(question: Question) {
    let isDeleted = false
    for (let i = 0; i <= this.questions.length - 1; i++) {
      if (this.questions[i].id_question === question.id_question) {
        this.questions.splice(i, 1);
        isDeleted = true
      }
      if (isDeleted) {
        this.questions[i].id_question += -1
      }
    }
  }

  onExport(){
    let theJSON = JSON.stringify(this.questions)
    let uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
    return uri;
  }

  downloadTitle()  :string{
    let date = new Date()
    return `Quizz-${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}.json`
  }

  goToHome() {
    this.router.navigate(['home'])
  }


}

