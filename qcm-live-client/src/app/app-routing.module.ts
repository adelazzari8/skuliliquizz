import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LobbyComponent } from './pages/lobby/lobby.component';
import { MenuComponent } from './pages/menu/menu.component';
import { PartyComponent } from './pages/party/party.component';
import { ResultatComponent } from './pages/resultat/resultat.component';
import { AuthGuardService } from './services/auth/auth-guard.service';
import {CreationQuizzComponent} from "./pages/creation-quizz/creation-quizz.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'create-quizz', component: CreationQuizzComponent},
  { path: 'menu', component: MenuComponent, canActivate: [AuthGuardService] },
  { path: 'party', component: PartyComponent, canActivate: [AuthGuardService] },
  { path: 'lobby/:id', component: LobbyComponent, canActivate: [AuthGuardService] },

  { path: 'resultat', component: ResultatComponent, canActivate: [AuthGuardService] },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
