import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client'

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  socket: any
  readonly url: string = "ws://100.74.7.85:3000"

  constructor() {
    this.socket = io.io(this.url, {
      reconnectionDelayMax: 10000
    })
  }

  listen(eventName: string): Observable<any> {
    return new Observable((Subscriber) => {
      this.socket.on(eventName, (data: any) => {
        Subscriber.next(data)
      })
    })
  }

  emit(eventName: string, data: any) {
    this.socket.emit(eventName, data)
  }

  join(groupName: string) {
    this.socket.emit('join', groupName);
  }
}
