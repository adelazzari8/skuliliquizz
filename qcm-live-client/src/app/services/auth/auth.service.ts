import { Injectable } from '@angular/core';
import { info } from 'src/assets/global-constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  public isAuth = () => {
    if (info.userId !== '-1') {
      return true
    }
    return false
  }
}
