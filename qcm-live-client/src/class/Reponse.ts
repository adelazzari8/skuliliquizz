export class Reponse {
  id: number;
  numero: number;
  labelle: string;
  constructor(id: number, numero: number, labelle: string) {
    this.id = id;
    this.numero = numero;
    this.labelle = labelle;
  }
}
