import { Reponse } from "./Reponse";

export class Question {
  done: boolean = false;
  id_question: number;
  labelle: string = "";
  url: string = "";
  reponses: Reponse[] = [];
  reponse_index: string = '-1';
  constructor(id_question : number ) {
    this.id_question = id_question
    for(let i = 0; i < 4; i++){
      this.reponses.push(new Reponse(i,i,""))
    }
  }
}

