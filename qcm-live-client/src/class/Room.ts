import User from "./User"

export default class Room {
  id: string
  name: string
  nbPlayers: number = 10
  nbTimes: number = 7
  nbQuestions: number = 10
  host: User
  users: User[] = [];

  themes: string[] = [];
  stat:number[] = [0, 0];

  constructor(id: string, name: string, host: User) {
    this.id = id;
    this.name = name
    this.host = host
  }

  addUser(user: User) {
    this.users.push(user)
  }
}
